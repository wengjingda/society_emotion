# -*- coding: utf-8 -*-
__author__ = 'k'
import numpy as np
import sys
import MySQLdb
import random
import copy

def getFileLines(path):
    '''读取文件中所有函数

    :param
        path：文件路径
    :return
        lines：包含文本内容的数组
    '''
    with open(path,'r') as f: lines = [ line.strip() for line in f.readlines()]
    f.close()
    return lines

def writeFile(path,lines):
        '''

        :param
            path：写入的文件路径
            lines：写入的文件内容
        :return:
            True：操作成功
        '''
    #try:
        f = open(path,'w')
        for line in lines:
            f.write(line.encode('utf-8')+"\n")
        f.flush()
        f.close()
        return True
    #except :
        return False

def getSpeechDic():
    '''获取词性权重表

    :return
        speechDic：词性权重表 speechDic[词性]=权重
    '''
    lines = getFileLines('../Data/speech.txt')
    speechDic = {}
    for i in lines:
        i = i.split('\t',2)
        speechDic[i[0]] = float(i[1])
    return speechDic

def connect():
    '''连接数据库

    :return
        conn：
        cursor：
    '''
    reload(sys)
    sys.setdefaultencoding('utf-8')
    conn=MySQLdb.connect(host="192.168.235.36",user="fig",passwd="fig",db="fig",charset = 'utf8')
    cursor =conn.cursor()
    return conn,cursor

def select(sql,t = None):
    '''返回数据表

    :return
        row：返回数数据表
    '''
    conn,cursor = connect()
    # 利用格式符修改sql语句
    if t != None:
        sql = sql % t
    cursor.execute(sql)
    row=cursor.fetchall()
    cursor.close()
    conn.close()
    return row


def quicksort(arr,left,right,tag):
    if left<right:
        dp = partition(arr, left, right,tag)
        quicksort(arr, left, dp - 1,tag)
        quicksort(arr, dp + 1, right,tag)

def partition(arr,left,right,tag):
        pivot = copy.deepcopy(arr[left])
        while left < right :
            while left < right and arr[right][tag] >= pivot[tag]:
                right-=1
            if left < right:
                arr[left] = copy.deepcopy(arr[right])
                left+=1
            while left < right and arr[left][tag] <= pivot[tag]:
                left+=1
            if left < right:
                arr[right] = copy.deepcopy(arr[left])
                right-=1

        arr[left] = copy.deepcopy(pivot)
        return left


def lcs(strA, strB):
    '''利用LCS求两个字符串的最大公共子串

    :param
        strA：字符串A
        strB：字符串B
    :return:
        strC：最大公共子串
    '''
    if strA==strB:
        return strA
    lenA=len(strA)
    lenB=len(strB)
    opt=np.zeros((lenA+1,lenB+1))
    for i in np.arange(lenA)+1:
        for j in np.arange(lenB)+1:
            if i!=0 and j!=0:
                if strA[i-1]==strB[j-1]:
                    opt[i][j] = opt[i-1][j-1] + 1
                else:
                    opt[i][j]=max(opt[i-1][j], opt[i][j-1])

    i=lenA
    j=lenB
    strC=[]
    while i>0 and j>0:
        if strA[i-1]==strB[j-1]:
            strC.append(strA[i-1])
            i-=1
            j-=1
        elif opt[i - 1][j] >= opt[i][j - 1]:
            i-=1
        else:
            j-=1
    return strC


def get_worddir():
    worddir={}
    flagdir={}
    speechdir=getSpeechDic()
    with open('../data/weight.txt') as ifile:
        for line in ifile:
            arr=line.split('\t',5)
            flagdir[arr[0]]=arr[1]
            if arr[1] in speechdir:
                worddir[arr[0]]=speechdir[arr[1]]*(1+float(arr[4]))
            else:
                worddir[arr[0]]=1
    return worddir,flagdir


if __name__ == '__main__': # 判断是否是主要运行模块
    dir = getSpeechDic()
    print dir
    '''
    lines = getFileLines('../Data/speech.txt')
    speechDic = {}
    for i in lines:
        i = i.split('\t',2)
        speechDic[i[0]] = float(i[1])
    for i in speechDic.keys():
        print i , speechDic[i]
    '''