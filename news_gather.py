# -*- coding: utf-8 -*-
# 将new聚集为event
__author__ = 'david'
from pandas import DataFrame,Series
import numpy as np
from datetime import datetime
import math
import time
import jieba.analyse
import jieba.posseg as pseg
import tool
import info_init
class news:
    '''
    news.id：新闻标号（long）
    news.title：新闻标题（arr_str）
    news.datetime：新闻发表时间（datetime）
    news.flag：为True时表示这是一个未归类的新闻
    news.content：新闻正文
    news.title_arr：新闻标题分词数组
    news.flag：新闻未被聚类时为真（boolean）
    '''
    def __init__(self,id,title,content,datetime):

        if u'-搜狐新闻' in title:
            title = title.split(u'-搜狐新闻')[0]
        if u'_新闻' in title:
            title = title.split(u'_新闻')[0]

        self.content=content
        self.title=title
        self.id = id
        self.title_arr = self.pseg_title(title)
        self.datetime = datetime
        self.flag = True

    def pseg_title(self,title):
        title_arr=[]
        for i in pseg.cut(title):
            title_arr.append(str(i.word))
        return title_arr


class event:
    '''
    event.name：事件名
    event.datetime：事件时间，等于新闻第一次发表时间
    event.newsList：事件包含的新闻列表（arr_news）
    event.err_flag：事件错误标志，为真时说明新闻聚类存在错误结果
    event.topKeywordNum：事件保留的关键词数目最大值，默认为50
    '''
    topKeywordNum=50
    def __init__(self,news):
        self.err_flag=False
        self.name = news.title
        self.datetime = news.datetime
        self.newsList  = []
        self.newsList.append(news)

    def printEvent(self):
        print datetime.strftime(self.datetime,"%Y-%m-%d %H:%M:%S"),
        for news in self.newsList:
            print news.title,'$'
        print

    def getIdLine(self):
        line = ''
        for news in self.newsList:
            line = line + "#" + str(news.id)
        return line

    def getLine(self):
        line = datetime.strftime(self.datetime,"%Y-%m-%d %H:%M:%S") + "$" +self.getIdLine()+ "$"
        for news in self.newsList:
            line = line + "#" + news.title
        return line

    def count_similar(self,news_j):
        '''利用相同关键词与最大公共子串长度计算事件与新闻的相似度

        对事件新闻列表中的每个新闻news_i:
            计算新闻news_i与新闻news_j的新闻标题分词交集intersection_ij
            对于交集intersection中的每个分词word：
                新闻i与新闻j的相似度similarity_ij+=词表中word的权重
            计算新闻i与新闻j标题中的最大公共子串LCS_ij
            alpha=1+最大公共子串的长度/新闻ij中标题长度的最大值
            相似度阈值limit=基础相似度阈值base_limit+新闻ij中标题长度的最大值/5
            如果similarity_ij*(1+alpha)>limit:
                相似新闻数similar_news_num+=1
        如果(相似新闻数/事件新闻数)>0.6:
            则认为事件与新闻相似，将新闻加入到事件的新闻列表中

        :param news_j：
        :return:
        '''
        similar_news_num=0
        for news_i in self.newsList:
            similarity=0
            similar_word_num=0
            for word in news_i.title_arr:
                if word in news_j.title_arr:
                    if word in info_init.worddir:
                        similar_word_num+=1
                        similarity+=info_init.worddir[word]
                    elif word in info_init.flagdir:
                        similarity+=info_init.speechdir.get([info_init.flagdir[word]],1)
            if similar_word_num>3:
                news_lcs=tool.lcs(news_i.title,news_j.title) # 最大相同子串
                similarity = similarity * (1+len(news_lcs)/max(len(news_i.title_arr),len(news_j.title_arr)))
            limit=info_init.similarity_limit+max(len(news_i.title_arr),len(news_j.title_arr))/5
            if  similarity>limit:
                similar_news_num+=1

        if float(similar_news_num)/len(self.newsList)>0.6: # 投票表决
            return True
        else:
            return False


if __name__ == '__main__': #

    print 'start time:  '+time.strftime("%Y-%m-%d %H:%M:%S")
    newsList = []
    sql='select * from news order by news_datetime asc limit 30000,20000'
    rows=tool.select(sql)
    for line in rows:
        newsList.append(news(long(line[0]),line[5],line[7],line[11])) # news为一个类
    print 'build News end time:  '+time.strftime("%Y-%m-%d %H:%M:%S")

    eventList = []
    for i in range(0,len(newsList)-1):
        sNews = newsList[i]
        if sNews.flag:
            tempEvent = event(sNews)
            sNews.flag = False
            for j in range(i+1,len(newsList)):
                eNews = newsList[j]
                delta=eNews.datetime - sNews.datetime
                if eNews.flag and abs(delta.days)<2:
                    if tempEvent.count_similar(eNews):
                        eNews.flag = False
                        tempEvent.newsList.append(eNews)
                elif abs(delta.days)>=2:
                    eventList.append(tempEvent)
                    break

    print 'build event end time:  '+time.strftime("%Y-%m-%d %H:%M:%S")
    outLine = []
    outLine_w=[]
    for e in eventList:
        outLine.append(e.getLine())
    tool.writeFile('../Data/event.txt',outLine)
