# -*- coding: utf-8 -*-
# 分析event信息
__author__ = 'david'
import tool
import time
import jieba.analyse
import jieba.posseg as pseg
import numpy as np
import  info_init
import math
from pandas import DataFrame,Series
class event:
    '''
    event.speechDir：词性权重字典
    event.topKeywordNum：抽取关键词最大数目
    event.tempDir：用于更新词典的临时词典
    '''
    topKeywordNum = info_init.topKeywordNum
    speechDir = tool.getSpeechDic()
    tempDir = {}
    def __init__(self,newsIdList,rows):
        '''创建新闻

        将新闻的标题，正文，时间信息添加到事件对应的数组
        并将新闻列表中最早新闻出现时间作为事件出现时间，将正文最多的新闻正文与标题作为事件正文与标题

        :param newsIdList：新闻ID列表
        :param rows：与事件相关的新闻信息数组,arr[id,'news_title','news_content',news_datetime]
        :return:
        '''
        self.rows=rows
        self.newsIdList = []
        self.newsIdList = newsIdList
        self.getNewsList()
        self.confirmEventTime()
        self.confirmEventTitle_Content()
        self.confirmEventKeywords()

    def getNewsList(self):
        self.newsTitleList = [] # 事件中的新闻标题列表
        self.newsContenList = [] # 事件中的内容列表
        self.newsDatetimeList = [] # 事件中的时间列表
        for row in self.rows:
            title = row[1]
            if u'-搜狐新闻' in title:
                title = title.split(u'-搜狐新闻')[0]
            if u'_新闻' in title:
                title = title.split(u'_新闻')[0]
            self.newsTitleList.append(title)
            self.newsContenList.append(row[2])
            self.newsDatetimeList.append(row[3])

    def confirmEventTime(self):
        self.datetime = min(self.newsDatetimeList)

    def confirmEventTitle_Content(self):
        MaxIndex = 0
        MaxLen = len(self.newsContenList[MaxIndex])
        for i in range(1,len(self.newsContenList)):
            if len(self.newsContenList[i]) > MaxLen:
                MaxIndex = i
                MaxLen = len(self.newsContenList[MaxIndex])
        self.title = self.newsTitleList[MaxIndex]
        self.content = self.newsContenList[MaxIndex]

    def confirmEventKeywords(self):
        '''按权重对事件关键词进行排序，保留权重前十大的关键词

        :return:
        '''
        self.keywordList = [] # 关键词列表
        self.keywordDir = {} # 筛选后剩下的关键词权重词典
        kwCountDir = {} # 所有关键词权重字典
        for i in range(0,len(self.newsTitleList)):
            words = jieba.analyse.extract_tags(self.newsTitleList[i].split(' ')[0],event.topKeywordNum)
            for word in words:
                for ws in pseg.cut(word):
                    if kwCountDir.has_key(ws.word):
                        kwCountDir[ws.word] = kwCountDir[ws.word]+ 1
                    else:
                        if event.speechDir.has_key(ws.flag):
                            if event.speechDir[ws.flag]>1.2:
                                kwCountDir[ws.word]=1

        tuList = sorted(kwCountDir.items(), reverse=True,key=lambda x:x[1]) # 排序比较keys
        if len(tuList) > info_init.keyword_num:
            right = info_init.keyword_num
        else:
            right = len(tuList)

        for i in range(0,right):
            self.keywordList.append(tuList[i][0])
            self.keywordDir[tuList[i][0]] = tuList[i][1]

    def printEventInfor(self):
        print self.datetime,'   ',self.title,'  ',
        for w in self.keywordList:
            print w,
        print

    def getId(self):
        idList = ''
        for i in self.newsIdList:
            idList = idList + '#' + i
        return idList

    def getLine(self,id):
        # 事件整合后只剩下，时间，事件id，新闻id，关键词词典
        line = str(self.datetime)+"$"+str(id)+"$"+self.getId()+"$"+self.title+"$"+str(self.keywordDir)
        return line


if __name__ == '__main__':
    print 'start time:  '+time.strftime("%Y-%m-%d %H:%M:%S")
    lines = tool.getFileLines('../Data/event.txt')
    lines.sort()

    sql = 'select news_id,news_title,news_content,news_datetime from news '
    rows = np.array(tool.select(sql))
    data = DataFrame(rows[:,1:],index=rows[:,0],columns=['news_title','news_content','news_datetime'])
    print 'sql end time:  '+time.strftime("%Y-%m-%d %H:%M:%S")

    outList = []
    event_id=0
    for line in lines:
        line = line.split('$',2)
        idList = line[1].split('#')[1:]
        temp_rows=[]
        for id in idList:
            temp_rows.append([int(id),data.ix[int(id)]['news_title'],data.ix[int(id)]['news_content'],data.ix[int(id)]['news_datetime']])
        e = event(idList,temp_rows)
        outList.append(e.getLine(event_id))
        event_id+=1
    outList.sort()
    tool.writeFile('../Data/eventInfor.txt',outList)

    outList = []
    for key in event.tempDir.keys():
        outList.append(key +'   ' +event.tempDir[key]) # 将一开始未出现的词写入词性表
    tool.writeFile('../Data/speechAdd.txt',outList)
    print 'end time:  '+time.strftime("%Y-%m-%d %H:%M:%S")
