#coding=utf-8
__author__ = 'david'
import tool
'''
news_gather
similarity_limit;新闻聚类相似度基础阈值
worddir:单词权重词典
flagdir:单词词性词典
speechdir:词性权重词典

'''
similarity_limit=4.5
speechdir=tool.getSpeechDic()
worddir,flagdir=tool.get_worddir()

'''
comfirm_event
keyword_num=10:事件保留的关键词数
topKeywordNum=50:事件抽取的关键词数
'''
keyword_num=5
topKeywordNum=50

'''
event_relative
iterate_num_max:主题迭代聚类次数最大值
DF_TE:主题与事件聚类时距离数组
DF_TT:主题与主题聚类时距离数组
alpha_TE:主题与事件聚类时的距离因子，距离等于=词典规模*距离因子
alpha_TT:主题与主题聚类时的距离因子
'''
iterate_num_max=10
DF_TE=[4+x*0.6 for x in range(8)]
DF_TT=[4+x*0.8 for x in range(8)]
alpha_TE=1.2
alpha_TT=1.4



