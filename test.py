# coding=utf-8
from __future__ import division
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import scipy.stats
import math
import pandas
import urllib2
import re
from bs4 import BeautifulSoup
import numpy as np
import jieba.posseg as pseg
import jieba as jb
import lda
import scipy.stats
import os
import os.path
from pandas import DataFrame,Series
import pandas as pd
import time
import MySQLdb
pattern1=re.compile('(#.+#)')
def pseg_word(news_array,stop_word):
    '''统计新闻标题分词信息

    :param
        news_array：新闻列表

    :return
        word_num_arr：新闻标题分词数字版
        count：统计词典
        word_2_tag：分词标号词典，{'单词':词号}
        tag_2_flag：分词词性数组，tag_2_flag[词号]==词性
        tag_2_word：分词标号数组，tag_2_word[词号]==单词
        flag_2_weight：词性权重词典，{'词性'：权重}

    '''
    all_word_num=0

    count={} # 统计词典 标号对应个数
    wordcount=0 # 词标号
    word_2_tag={} # 分词标号词典
    tag_2_flag=[] # 分词词性数组
    tag_2_word=[] # 分词标号数组
    word_num_arr=[]
    flag_2_weight=get_flag_2_wight('D:\Users\Administrator\PycharmProjects\lapose\data\speech.txt')

    for line in news_array:
        word_fillter=[]
        for i in pseg.cut(line): # 这里是切分标题并转换为数字表示
            # if str(i.word) not in stop_word:
            all_word_num+=1
            if str(i.word) not in count:
                print str(i.word)
                count[str(i.word)]=1
                word_2_tag[str(i.word)]=wordcount
                tag_2_word.append(str(i.word))
                tag_2_flag.append(str(i.flag))
                word_fillter.append(wordcount)
                wordcount=wordcount+1
            if str(i.word) in count:
                count[str(i.word)]=count[str(i.word)]+1
                word_fillter.append(word_2_tag[str(i.word)])
        word_num_arr.append(word_fillter)
    return word_num_arr,count,word_2_tag,tag_2_flag,tag_2_word,flag_2_weight


def get_flag_2_wight(filepath=''):
    '''获取权重词典，如果没有传入路径，则返回默认词典

    :param
        filepath：权重词典文件路径
    :return
        flag_2_weight：词性权重数组
    '''
    flag_2_weight={
        'n':0.18,        'v':0.18,
        't':0.08,        's':0.08,
        'f':0.08,        'a':0.12,
        'b':0.05,        'z':0.01,
        'r':0.06,        'm':0.08,
        'q':0.05,        'nrt':0.18,
        'd':0.05,        'p':0.02,
        'c':0.02,        'u':0.01,
        'e':0.01,        'y':0.01,
        'x':0.02,        'nt':0.18,
        'nr':0.18,       'nz':0.18,
        'vn':0.18,       'ad':0.07,
        'ns':0.18,       'vn':0.18,
        'j':0.15,        'eng':0.05
    } # 词性权重词典

    if filepath:
        f_2_w_df=pd.read_table(filepath)
        f_2_w_df.columns=['flag','weight','info']
        for i in range(len(f_2_w_df.index)):
            flag_2_weight[f_2_w_df.ix[i]['flag']]=f_2_w_df.ix[i]['weight']/10 # 权重过大将其调小

    return flag_2_weight


def get_similarity_matrix(news_word,tag_2_flag,flag_2_weight,limit,count,tag_2_word): # 获得相似矩阵
    '''获得相似矩阵

    :param
        news_word：新闻内容标号数组
        tag_2_flag：分词词性数组，tag_2_flag[词号]==词性
        flag_2_weight: 词性权重词典，flag_2_weight['词性']==权重
        limit：相似度阈值，只有相似度大于这个值才认为两条新闻相似
    :return
        np.sign(similarity_matrix_vsm)：相似矩阵，第i条新闻与第j条新闻相似，则array[i][j]==array[j][i]==1，否则为0
        similarity_matrix_wetigh：相似度矩阵，similarity_matrix_wetigh[i][j]表示第i，j条新闻的相似度

    '''
    news_num=len(news_word)

    similarity_matrix_vsm=boolean_array=[([False] * news_num) for i in range(news_num)]
    similarity_matrix_wetigh=np.zeros((news_num,news_num))
    for i in range(news_num):
        for j in range(i+1,news_num):
            similarity_matrix_wetigh[i][j]=calculate_similarity(news_word[i],news_word[j],tag_2_flag,flag_2_weight,count,tag_2_word)
            similarity_matrix_wetigh[j][i]=similarity_matrix_wetigh[i][j]
            if similarity_matrix_wetigh[i][j]>limit:
                similarity_matrix_vsm[i][j]=True
                similarity_matrix_vsm[j][i]=True
    return similarity_matrix_vsm,similarity_matrix_wetigh


def calculate_similarity(news_i,news_j,tag_2_flag,flag_2_weight,count,tag_2_word):
    '''计算两条新闻的相似性

    :param
        news_i：第i条新闻的新闻内容标号数组
        news_j：第j条新闻的新闻内容标号数组
        tag_2_flag：分词词性数组，tag_2_flag[词号]==词性
        flag_2_weight: 词性权重词典，flag_2_weight['词性']==权重
    :return
        similarity：新闻相似度
    '''
    similarity=0
    intersection_array=np.intersect1d(news_i,news_j) # 这里求交集
    news_lcs=lcs(news_i,news_j)
    for intersection in intersection_array:
        # 根据词频调节权重
        similarity+=(flag_2_weight.get(tag_2_flag[intersection],0.05)-5*count[tag_2_word[intersection]]/len(tag_2_word))
    # 利用最大公共子串来调节权重
    similarity=similarity*(1.2+len(news_lcs)/max(len(news_i),len(news_j)))
    return similarity


def get_reachability_matrix(similarity_matrix):
    '''获得可达矩阵

    若新闻1与新闻2相似，新闻2与新闻3相似，但新闻1与新闻3不相似，则认为两者可达

    :param
        similarity_matrix：相似矩阵，如新闻i，j相似则similarity_matrix[i][j]==1
    :return
        reachability_matrix：可达矩阵，如新闻i，j可达则reachability_matrix[i][j]==1
    '''
    news_num=len(similarity_matrix)
    reachability_matrix=np.array(similarity_matrix)
    for k in range(news_num):
        for i in range(news_num):
            for j in range(news_num):
                if similarity_matrix[i][j]==False:
                    if similarity_matrix[i][k] and similarity_matrix[k][j]:
                        reachability_matrix[i][j]=True
    return reachability_matrix


def get_cluser_result(reachability_matrix): # 返回聚类结果
    '''根据可达矩阵，将彼此可达的新闻划分成一类，返回聚类结果

    :param
        reachability_matrix：可达矩阵，若第i，j跳新闻可达则reachability_matrix[i][j]==1
    :return
        返回聚类结果数组，其中0值为离散点
    '''
    news_num=len(reachability_matrix) # 新闻长度
    cluser_result=np.zeros(news_num,dtype=float)-1 # 聚类结果数组
    cluser_num=1.0 # 聚类结果标记
    for i in range(news_num):
        temp_tag=0
        if cluser_result[i]==-1:
            for j in range(news_num):
                if reachability_matrix[i][j]==1:
                    temp_tag=1
                    cluser_result[j]=cluser_num
            if temp_tag:
                cluser_num +=1
    return cluser_result-cluser_result.min()


def print_news(labels,news_title,stop_word): # 打印结果
    '''根据聚类结果，显示新闻

    :param
        labels：聚类结果
        news_title：新闻内容数组
    :return

    '''
    for i in np.unique(labels):
        print i
        string_arr=news_title[labels==i]
        for j in range(len(string_arr)):
            print string_arr[j],
            words=pseg.cut(string_arr[j])
            for w in words:
                if str(w.word) not in stop_word:
                    print w.word, w.flag,
            print
        print '===='*5


def connect_mysql(server,username,userpasswd,dbname,sql):
    '''连接数据库获取关系表
    :param
        server：主机名
        username：用户名
        userpasswd：用户密码
        sql：查询语句
    :return
        results：关系表
    '''
    try:
        conn = MySQLdb.connect(host=server, user=username, passwd=userpasswd, port=3306, charset='utf8') # 设置主机，账号，密码
        cur = conn.cursor()
        conn.select_db(dbname) # 连接的数据库
        # 通过sql语句选择想要分析的新闻类别
        # count = cur.execute("select * from tb_yiou_news where field='金融'")
        count = cur.execute(sql)
        print 'there has %s rows record' % count
        print '==' * 10
        cur.scroll(0, mode='absolute')

        # 获得返回信息
        results = cur.fetchall()

        conn.commit()
        cur.close()
        conn.close()
    except MySQLdb.Error, e:
        print "Mysql Error %d: %s" % (e.args[0], e.args[1])
    return results


def lcs(strA, strB):
    '''利用LCS求两个字符串的最大公共子串

    :param
        strA：字符串A
        strB：字符串B
    :return:
        strC：最大公共子串
    '''
    if strA==strB:
        return strA
    lenA=len(strA)
    lenB=len(strB)
    opt=np.zeros((lenA+1,lenB+1))
    for i in np.arange(lenA)+1:
        for j in np.arange(lenB)+1:
            if i!=0 and j!=0:
                if strA[i-1]==strB[j-1]:
                    opt[i][j] = opt[i-1][j-1] + 1
                else:
                    opt[i][j]=max(opt[i-1][j], opt[i][j-1])

    i=lenA
    j=lenB
    strC=[]
    while i>0 and j>0:
        if strA[i-1]==strB[j-1]:
            strC.append(strA[i-1])
            i-=1
            j-=1
        elif opt[i - 1][j] >= opt[i][j - 1]:
            i-=1
        else:
            j-=1
    return strC


def get_stop_word(file_path): # 获取停用词
    '''输入停用词词典路径，获取停用词

    :param
        file_path：停用词词典路径
    :return
        stopwords：包含停用词信息的哈希集合
    '''
    Test_Data = open(file_path, "r")  # 读取文件
    stopwords = set()
    for line in Test_Data:
        stopwords.add(line.strip())
    Test_Data.close()  # 关闭文件
    return stopwords


def road_text(filepath):
    '''从txt文件中读取数据集

    return：
        result：包含新闻标题的数组
    '''
    result=[]
    with open(filepath) as ifile:
        for line in ifile:
            result.append(line)
    return result


sql='select * from news LIMIT 500 '
rows=np.array(connect_mysql("192.168.235.36","fig","fig","fig",sql)) # 输入主机名 用户名 密码 库名 sql语句
news_title=rows[:,5]
limit=1
stop_word=get_stop_word('../data/stopwords.txt') # 获取停用词
'''
news_title=road_text('../data/event_t.txt')
'''
word_num_arr,count,word_2_tag,tag_2_flag,tag_2_word,flag_2_weight=pseg_word(news_title,stop_word)
similarity_matrix,similarity_matrix_weiht=get_similarity_matrix(word_num_arr,tag_2_flag,flag_2_weight,limit,count,tag_2_word)
reachability_matrix=get_reachability_matrix(similarity_matrix)
cluser_result=get_cluser_result(reachability_matrix)
print_news(cluser_result,np.array(news_title),stop_word)
'''

boolean_array=[([False] * 4) for i in range(4)]
print boolean_array
boolean_array[1][2]=True
print boolean_array
print type(boolean_array)

'''

