# -*- coding: utf-8 -*-
import math
import os
import os.path
import jieba.analyse
import copy
import jieba.posseg as pseg
__author__ = 'david'
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from news_cluser_finshed import tool

class paragraph():

    keyword_num=8

    def __init__(self, title, content, para_time,scale_num,possiion,kw_list):
        self.flag = False
        self.possiion=possiion
        self.datetime = para_time
        self.content = content
        self.title = title
        self.kw_list=kw_list
        self.scale_num=scale_num

    def get_keywords(self):
        line='@'
        for word in self.kw_list:
            line = line + word + "@"
        return line

    def get_line(self):
        return "#".join([self.title,self.content,str(self.possiion),self.get_keywords()])


class para_group:

    keyword_num=10

    def __init__(self,paragraph):
        self.independence_flag=True
        self.flag = False
        self.content_list=[]
        self.title_list=[]
        self.scale_num=paragraph.scale_num
        self.content_list.append(paragraph.content)
        self.title_list.append(paragraph.title)
        self.kw_dic = dict(zip(paragraph.kw_list,[1]*len(paragraph.kw_list)))

    def add_para(self,paragraph):
        self.title_list.append(paragraph.title)
        self.content_list.append(paragraph.content)
        for word in paragraph.kw_list:
            if word in self.kw_dic:
                self.kw_dic[word] += 1
            else:
                self.kw_dic[word] = 1
                self.scale_num += 1

    def get_line(self):
        return "!"+str(len(self.content_list))+"#".join(self.content_list)+"@".join(self.kw_dic.keys())

    def merge_group(self,group_j):

        for title in group_j.title_list:
            self.title_list.append(title)
        for content in group_j.content_list:
            self.content_list.append(content)
        for word in group_j.kw_dic.keys():
            if word in self.kw_dic:
                self.kw_dic[word] += 1
            else :
                self.kw_dic[word] = 1
                self.scale_num += 1

    @staticmethod
    def update_kw_dic(kw_dic):
        kw_list = sorted(kw_dic.items(), reverse=True,key=lambda x:x[1])
        if len(kw_list) > para_group.keyword_num:
            right = para_group.keyword_num
        else:
            right = len(kw_list)
        kw_dic={}
        for i in range(0,right):
            kw_dic[kw_list[i][0]] = kw_list[i][1]
        return kw_dic

    @staticmethod
    def calculate_sim(group,paragraph):
        similarity=0
        for word in paragraph.kw_list:
            if word in group.kw_dic:
                similarity+=weight[group.kw_dic[word]]
        if similarity>group.scale_num*0.3:
            return True
        else:
            return False

    @staticmethod
    def calculate_sim_groups(group_i,group_j):
        similarity=0
        for word in group_j.kw_dic.keys():
            if word in group_i.kw_dic:
                similarity+=weight[group_i.kw_dic[word]]
        if similarity>3.3:
            return True
        else:
            return False

def iterate_cluser(old_group_list):

    for group_i in old_group_list:
        group_i.flag=False
        group_i.kw_dic=para_group.update_kw_dic(group_i.kw_dic)

    is_change_flag=False
    new_topiclist=[]
    for i in range(len(old_group_list)):
        group_i=old_group_list[i]
        if not group_i.flag:
            group_i.flag=True
            for j in range(i+1,len(old_group_list)):
                group_j=old_group_list[j]
                if i==j:
                    continue
                if not group_j.flag:
                    if not group_i.independence_flag or not group_j.independence_flag:
                        if para_group.calculate_sim_groups(group_i,group_j):
                            is_change_flag=True
                            group_j.flag=True
                            group_i.independence_flag=False
                            group_j.independence_flag=False
                            group_i=copy.copy(old_group_list[i])
                            group_i.merge_group(group_j)
            new_topiclist.append(group_i)
    return new_topiclist,is_change_flag


if __name__ == '__main__':

    rootdir = '../data/paris/'
    weight = [math.log(x, 3) for x in range(1, 100)]
    para_list = []
    group_list = []
    for parent, dirnames, filenames in os.walk(rootdir):
        for filename in filenames:
            ifile = open(os.path.join(parent,filename))
            lines = ifile.readlines()
            ifile.close()
            para_time=lines[1]
            title=lines[2]
            for i in range(3,len(lines),2):
                if len(lines[i].strip())>30:
                    content=lines[i].strip('\n')
                    words = jieba.analyse.extract_tags(content,paragraph.keyword_num)
                    para_list.append(paragraph(title, content, para_time,len(words),i/2,words))

    for i in range(len(para_list)-1):
        paragraph_i=para_list[i]
        if not paragraph_i.flag:
            temp_group = para_group(paragraph_i)
            paragraph_i.flag=True
            for j in range(i, len(para_list)):
                paragraph_j=para_list[j]
                if not paragraph_j.flag:
                    if para_group.calculate_sim(temp_group, paragraph_j):
                        temp_group.add_para(paragraph_j)
                        temp_group.independence_flag=False
                        paragraph_j.flag=True
            group_list.append(temp_group)

    iterate_num=0
    is_change_flag=True
    new_grouplist=group_list
    while is_change_flag and iterate_num < 5:
        iterate_num += 1
        new_grouplist, is_change_flag = iterate_cluser(new_grouplist)
        print len(new_grouplist)

    outline=[]
    for group in new_grouplist:
        outline.append(group.get_line())
    tool.writeFile('../Data/para_group.txt',outline)