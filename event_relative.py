# -*- coding: utf-8 -*-
__author__ = 'david'

import time
from datetime import  datetime
import jieba.posseg as pseg
import tool
import math
import copy
import info_init


class event:
    def __init__(self,id,datetime,idList,title,kwDir):
        self.id = id
        self.datetime = datetime
        self.idList = idList
        self.title = title
        self.kwDir = kwDir
        self.flag = True


class topic:
    '''
    topic.flag：聚类标志，当未被聚类时为真
    independence_flag：独立标志，当话题中只有一个事件时为真
    '''
    def __init__(self,event):
        self.flag=True
        self.independence_flag=True
        self.datetime=event.datetime
        self.titleList = []
        self.kwDir = {}
        self.idList = []
        self.titleList.append(event.title)
        self.kwDir = event.kwDir
        self.idList.append(event.id)

    def getTopicTitleLine(self):
        line = ''
        if len(self.titleList) > 1:
            line = line +  "@" +str(len(self.titleList))
        for title in self.titleList:
            line = line + '  ' + title.replace(' ',':')
        return line.decode('utf-8')

    def getLine(self):
        line = str(self.idList[0])
        for i in range(1,len(self.idList)):
            line = line + ','+ str(self.idList[i])
        return line

    def append_event(self,new_event):
        '''将事件添加到主题中

        将主题的独立标志置为False，将事件的标题，id添加到主题的相关列表中
        对于事件词典的每个关键词word：
            如果word存在于主题关键词词典中：
                关键词权重=（word在主题中的权重+word在事件中的权重）*0.8+2
            如果word不存在于主题关键词词典中：
                关键词权重=1

        :param new_event:：被添加的事件
        :return:
        '''
        self.independence_flag=False
        self.titleList.append(new_event.title)
        self.idList.append(new_event.id)
        for key in new_event.kwDir.keys():
            if self.kwDir.has_key(key):
                self.kwDir[key] = (self.kwDir[key]+new_event.kwDir[key])*0.8+2
            else:
                self.kwDir[key] =  1

    def filter_keyword(self):
        '''过滤关键词，只在关键词词典中保留权重前10的关键词

        :return:
        '''
        if len(self.kwDir.values())<10:
            return
        tuList = sorted(self.kwDir.items(), reverse=True,key=lambda x:x[1])
        new_kwDic={}
        for i in range(10):
            new_kwDic[tuList[i][0]]=tuList[i][1]
        self.kwDir=new_kwDic

    def append_topic(self,new_topic):
        '''合并主题

        将新主题标题列表中的标题，与id列表中的id添加到原主题相应数组中
        对于新主题词典的每个关键词word：
            如果word存在于旧主题关键词词典中：
                关键词权重=（word在旧主题中的权重+word在新主题中的权重）*0.8+2
            如果word不存在于旧主题关键词词典中：
                关键词权重=1

        :param new_topic:
        :return:
        '''
        self.independence_flag=False
        for title in new_topic.titleList:
            self.titleList.append(title)
        for id in new_topic.idList:
            self.idList.append(id)
        for key in new_topic.kwDir.keys():
            if self.kwDir.has_key(key):
                self.kwDir[key] = (self.kwDir[key]+new_topic.kwDir[key])*0.8+2
            else:
                self.kwDir[key] =  1


def countSimilar(dir_1,dir_2, df, log_values,alpha=1.0):
    '''计算两个关键词词典的相似度

    对于词典1中的每个词word:
        如果word存在于词典2：
            value=关键词在两个词典中的权重总和
            如果value大于20：
                value=20
            相似度similarity+=log(value)*（1+word在词表中的权重）
    如果词典大小小于8：
        相似度阈值F=df[词典大小]
    否则：
        相似度阈值=词典大小*alpha
    如果相似度大于阈值：
        认为两个词典相似

    :param dir_1：关键词词典1
    :param dir_2：关键词词典2
    :param df：距离数组，记录当词典数目小于8的距离阈值
    :param log_values：log_values[i]=math.log(i+1,2)
    :param alpha：距离因子，用于调整当词典数目大于8时的距离阈值
    :return:
    '''
    dLen1 = len(dir_1.keys())
    dLen2 = len(dir_2.keys())

    if dLen1 ==0 or dLen2 == 0:
        return False
    if dLen2 > dLen1:
        max_len = dLen2 # maxlen
    else:
        max_len = dLen1
    if max_len == 1:
        if dir_1.keys()[0] == dir_2.keys()[0]:
            return True
        else:
            return False
    else:
        if max_len < 8:
            distance = df[max_len]
        else:
            distance = max_len*alpha

        similarity = 0.0 # d1,d2事件相似度
        for key in dir_2.keys():
            if key in dir_1:
                value = int(dir_1[key] + dir_2[key])
                if value > 20:
                    value = 20
                similarity += log_values[value]*(1+info_init.worddir.get(key, 0))

        if similarity >= distance:
            return True
        else:
            return False


def iterate_cluser(old_topic_list, alpha):
    '''迭代聚类

    将主题列表所有主题状态更新为未聚类
    从主题列表中抽取主题topic_i和主题topic_j，使两个主题事件列表长度不同时为1
        保留topic_i和topic_j权重前十的关键词
        利用countSimilar（topic_i.kwDir,topic_j.kwDir）计算主题相似度，除了alpha调整为1.3+迭代次数/10外其他都相同
        如果两主题相似：
            if_change_flag置为True
            将主题topic_j添加到topic_i中

    :param old_topic_list：为聚类的主题列表
    :param alpha：距离因子，用于调整相似度阈值
    :return:
            new_topiclist：新生成的主题数组
            is_change_flag：如果有主题被重新聚类，置为True
    '''
    log_values=[math.log(x,2) for x in range(1,100)]
    for topic_i in old_topic_list:
        topic_i.flag=True
        topic_i.filter_keyword()

    temp_flag=False
    topic_list=[]
    for i in range(len(old_topic_list)):
        topic_i=old_topic_list[i]
        if topic_i.flag:
            topic_i.flag=False
            for j in range(i+1,len(old_topic_list)):
                topic_j=old_topic_list[j]
                if i==j:
                    continue
                if topic_j.flag:
                    if not topic_i.independence_flag or not topic_j.independence_flag:
                        if countSimilar(dir_1=topic_i.kwDir,dir_2=topic_j.kwDir,df=info_init.DF_TT,log_values=log_values,alpha=alpha):
                            temp_flag=True
                            topic_j.flag=False
                            topic_i.independence_flag=False
                            topic_j.independence_flag=False
                            topic_i=copy.copy(old_topic_list[i])
                            topic_i.append_topic(topic_j)
            topic_list.append(topic_i)

    return topic_list ,temp_flag


if __name__ == '__main__':

    log_values=[math.log(x,2) for x in range(1,100)]
    lines = tool.getFileLines('../Data/eventInfor.txt')
    eventList = [] # 事件列表
    topicList = [] # 主题列表
    for line in lines:
        line = line.split('$',4) # 0-时间 1-事件标号 2-新闻标号 3-事件标题 4-事件关键词词典
        if len(eval(line[4]).values()) == 0:
            continue
        eventList.append(event(id=int(line[1]),datetime=line[0],idList=line[2].split('#')[1:],title=line[3],kwDir=eval(line[4]))) # 事件列表
    print 'built event end time:  '+time.strftime("%Y-%m-%d %H:%M:%S")

    for i in range(0,len(eventList)-1) :
        sEvent = eventList[i]
        if sEvent.flag:
            tempT = topic(sEvent)
            eventList[i].flag = False
            for j in range(i+1,len(eventList)):
                eEvent = eventList[j]
                if eEvent.flag:
                    if countSimilar(tempT.kwDir,eEvent.kwDir,info_init.DF_TE,log_values,alpha=info_init.alpha_TE):
                        tempT.append_event(eEvent)
                        eventList[j].flag = False
            topicList.append(tempT)
    print 'built topic end time:  '+time.strftime("%Y-%m-%d %H:%M:%S")

    print len(topicList)
    new_topiclist=topicList
    is_change_flag=True
    iterate_num=0
    print 'rebuilt topic start time:  '+time.strftime("%Y-%m-%d %H:%M:%S")
    while is_change_flag and iterate_num < info_init.iterate_num_max:
        iterate_num += 1
        new_topiclist, is_change_flag = iterate_cluser(new_topiclist,alpha=info_init.alpha_TT+iterate_num/10.0)
        print len(new_topiclist)
    print 'rebuilt topic end time:  '+time.strftime("%Y-%m-%d %H:%M:%S")

    outLine = []
    titleOutLine = []
    for t in new_topiclist:
        outLine.append(t.getLine())
        titleOutLine.append(t.getTopicTitleLine())
    tool.writeFile('../Data/topic.txt',outLine)
    tool.writeFile('../Data/topicTitle.txt',titleOutLine)
